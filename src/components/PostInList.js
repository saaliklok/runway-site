import React from "react";
import PropTypes from "prop-types";
import { Link } from "gatsby";
import PreviewCompatibleImage from "./PreviewCompatibleImage";
import styled from "@emotion/styled";

const Article = styled.article`
  margin: 0 30px;
`;

const HeadSection = styled.header`
  padding: 30px 0;
`;

const ArticleTitle = styled.h4`
  margin: 20px 0;
`;

const ArticleDate = styled.p``;

const Excerpt = styled.p``;

const ImageParent = styled.div`
  max-width: 300px;
`

const PostInList = props => {
  return (
    <div key={props.post.id}>
      <Article>
        <HeadSection>
          {props.frontmatter.featuredimage ? (
            <ImageParent>
            <PreviewCompatibleImage
              imageInfo={{
                image: props.frontmatter.featuredimage,
                alt: `featured image thumbnail for post ${props.frontmatter.title}`
              }}
            />
            </ImageParent>
          ) : null}
          <ArticleTitle className="post-meta">
            <Link
              className="title has-text-primary is-size-4"
              to={props.post.fields.slug}
            >
              {props.frontmatter.title}
            </Link>
          </ArticleTitle>
          <ArticleDate className="subtitle is-size-5 is-block">
            {props.frontmatter.date}
          </ArticleDate>
        </HeadSection>
        <Excerpt>{props.post.excerpt}</Excerpt>
        <Link to={props.post.fields.slug}>Keep Reading →</Link>
        <hr/>
      </Article>
    </div>
  );
};

PostInList.propTypes = {
  frontmatter: PropTypes.any,
  post: PropTypes.any
};

export default PostInList;
