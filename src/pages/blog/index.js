import React from 'react'
import styled from '@emotion/styled'

import Layout from '../../components/Layout'
import BlogRoll from '../../components/BlogRoll'

const HeaderSection = styled.div`
  display: flex;
  height: 150px;
  justify-content: space-around;
  align-items: left;
  flex-direction: column;
  font-family: 'Dosis';
  letter-spacing: 0.5px;
`

const Title = styled.h1`
  background-color: #1CC6B7;
  color: #fafafa;
  padding: 0.25em;
`

export default class BlogIndexPage extends React.Component {
  render() {
    return (
      <Layout>
        <HeaderSection
          className="full-width-image-container margin-top-0"
          style={{
            backgroundImage: `url('/img/blog-index.jpg')`,
          }}
        >
          <Title
            className="is-size-1"
          >
            Latest Stories
          </Title>
        </HeaderSection>
        <section className="section">
          <div className="container">
            <div className="content">
              <BlogRoll />
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}
