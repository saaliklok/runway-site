---
templateKey: blog-post
title: Runway Finance - Achieving Financial Stability
date: 2020-03-19T15:04:00.000Z
description: 'Achieve financial stability on your terms. '
featuredpost: false
featuredimage: /img/blog-index.jpg
tags:
  - personal finance
  - investing
---


At the beginning of each year, we all hope to turn the page and start fresh with a list of new goals to achieve for the year. This usually includes losing some amount of weight, learning some type of new skill, or maybe cutting out some crappy habits. Somewhere on that list is also cutting back on expenses and saving more money. As most of us know, cutting back and saving money is easier said than done, especially if you come from a background where any discussion of money is considered taboo. Most the decisions that we have to make are inherently financial decisions, so why don’t we learn about such topics in our universities or even in our homes? Because of this lack of accessibility to common personal finance knowledge, a lot of recent graduates and millennials have to deal with a host of personal finance problems, including low credit scores, a fear of investing, low savings, and crippling debt.

For those of you beginning your journey to financial stability, it's hard to know where to start. We hear terminology like 401(k) plans, index funds, and mutual funds and we feel trapped in a sea of options to the point where we do nothing. To those who feel this way, please know that the path to a financially stable life is not a one-day trek. Nobody expects you to figure it out today, it is something to work towards day by day.

Personal finance is not a one-size-fits-all topic. As the name suggests, personal finance is *personal**.***The financial decisions a millennial in Los Angeles makes are not the same types of decisions a college student in Boston should make. However, the lack accessible knowledge prevents all types of people from having the basic templates to follow.

Everyone wants to achieve financial freedom, but where should we start? By taking a good, hard look at what we spend on a daily, weekly, and monthly basis, we can make the necessary changes and improvements to ensure financial freedom. We can conquer intimidating finance terminology and take advantage of financial products available to us. We don't have to be afraid of mutual funds, 401(k)’s, stocks, bonds, budgets, and credit cards. In a world where all the knowledge in the world is at our fingertips, nothing can stop us from achieving your version of financial freedom.

Personal finance is a compilation of several topics, but they all have one common theme: ***discipline.*** If you can learn to be financially disciplined, you can overcome any financial hurdle that you encounter. After all, financial stability starts with you. You are responsible for the changes you want to make. Don’t let yourself be overwhelmed by all the choices. Your path may start today, but it certainly doesn’t end today.

You are the pilot sitting on the runway on your way to financial freedom. How will you choose to get there?